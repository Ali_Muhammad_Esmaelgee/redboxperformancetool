package configurableProductsImageResize;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;


public class imageHover {
	
	static WebDriver driver;
	static DesiredCapabilities cap;
	
	static String errors;

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		String baseUrl = "https://www.sephora.ae/";
		String FileUrls = "productUrls.txt";
		String productUrl="";
		List<String> productUrls = new ArrayList<String>(); 
		String productName="";
			
		//Setting Timeout
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		cap = new DesiredCapabilities();
		cap.setJavascriptEnabled(true);
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "phantomjs-2.1.1-windows/bin/phantomjs.exe");
		String[] phantomArgs = new  String[] {
			    "--webdriver-loglevel=NONE"
			};
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, phantomArgs);
		WebDriver driver = new PhantomJSDriver(cap);
		
		
		//Authenticating htaccess
//		driver.get("https://sephora:eXBN4nurFZB4BNp4@www.sephora.ae/");
		
		//Loop through list ProductUrls list
		try{			
			 FileReader fileReader = new FileReader(FileUrls);
			 BufferedReader bufferedReader = new BufferedReader(fileReader); 
			 
			 int j=1;
			 
			 System.out.println(">>>No Associated Products on FE for following products: ");
			 
			 while((productUrl = bufferedReader.readLine()) != null) {				 
				 
				 try{
				
				//Go to Product Page
				driver.get(productUrl);
				 
				productName = driver.findElement(By.xpath("//*[@id='product_addtocart_form']/div[2]/h1/div")).getText();							
				
				 
				//Get no of product images
				int noOfOptions = driver.findElements(By.xpath("//*[@id='product-options-wrapper']/dl/dd/div//ul/li")).size();
				
				System.out.println(productName + " - " + noOfOptions);
				
				if(noOfOptions <= 0){
					System.out.println(">>>Product Name: " + productName + "("+productUrl+")");										
				}
				
				//Loop through each image
//				for(int i=0; i < noOfOptions ; i++){
//					//click image
//					driver.findElement(By.xpath("//*[@id='product-options-wrapper']/dl/dd/div//ul/li["+(i+1)+"]/a")).click();
//					
//					//wait for image to show					
//					try{
//						while(driver.findElement(By.xpath("//div[@class='product-image-gallery loading']")).isDisplayed()){			
//							Thread.sleep(1000);
//						}
//					}catch(Exception e){
////						File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
////						FileUtils.copyFile(scrFile, new File("image"+(i+1)+".jpg"), true);
//						
//						System.out.print(
//									driver.findElement(By.xpath("//*[@id='product-options-wrapper']/dl/dd/div//ul/li["+(i+1)+"]/a")).getAttribute("id")
//									+ ", ");						
//					}
//				}
				
				
				 }catch(Exception e){
					 System.out.println(">>>Error:\nProduct Url: " + productUrl + "\nCannot be found\n");
				 }
				
			 }			 
			
			
		}catch(FileNotFoundException err){			System.out.println("Unable to open file '" + FileUrls + "'");  
		} catch (IOException e) {			e.printStackTrace();
		}
		
		

		
		
		
		//report no of successful resize
		
		
	}

}
