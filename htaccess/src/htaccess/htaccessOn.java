package htaccess;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.hamcrest.core.SubstringMatcher;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.sun.media.sound.AlawCodec;

import javafx.scene.control.Alert;


public class htaccessOn {

	
	static WebDriver driver;
	static DesiredCapabilities cap;
	static File scrFile;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String baseUrl;
		String fileUrls = "fileUrls.txt";
		String url, line="";
		int htaccess;
		
		cap = new DesiredCapabilities();
		
		
		//silent phantom js warnings
		cap.setJavascriptEnabled(true);
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "../phantomjs-2.1.1-windows/bin/phantomjs.exe");
		String[] phantomArgs = new  String[] {
			    "--webdriver-loglevel=NONE"
			};
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, phantomArgs);
		
		
		//read url files
		try{
			FileReader fileReader = new FileReader(fileUrls);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			
			while((line=bufferedReader.readLine()) != null){
				
				url = line.substring(0, line.indexOf(" htaccess="));
				htaccess = Integer.parseInt(line.substring(line.indexOf("htaccess=")+9));
				
				//Test for htaccess only if htaccess in file = 1
				if(htaccess == 1){					
					assertHtAccess(url);
				}

			}
			
			
		}catch(FileNotFoundException err){			System.out.println("Unable to open file '" + fileUrls + "'");  
		}catch (IOException e) {			e.printStackTrace();
		}
		
	}
	

	public static int assertHtAccess(String url){		
		
		//Re initialising 'driver' to restart with new session each time		
		WebDriver driver = new PhantomJSDriver(cap);
	
		driver.get(url);	
		
//			uncomment this for screenshot
//			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//			FileUtils.copyFile(scrFile, new File("image.jpg"), true);
		scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
//		System.out.println(scrFile.length());
		
		if(scrFile.length() < 1000){
			System.out.println("url: " + url + "\t-\t Protected");						
		}else{
			System.out.println(">>>url: " + url + "\t-\t NOT PROTECTED");						
		}
	
		driver.quit();
		
		
		return 0;
	}

}
