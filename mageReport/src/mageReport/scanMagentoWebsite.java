package mageReport;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import junit.framework.Assert;

public class scanMagentoWebsite {

	
static WebDriver driver;
	
	static DesiredCapabilities cap;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = new PhantomJSDriver();
		String baseUrl = "https://www.magereport.com/";
		String firstWebsite = "https://www.alexandra.co.uk/";
		String fileName = "ProjectList.txt";
		String line = "";
		String currResult = "";
		List<String> websites = new ArrayList<String>();
		List<String> results = new ArrayList<String>();
		String currentPatch = "";
		String currentPatchStatus = "";
		
		int noOfResult;
		
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
//		//open website
		driver.get(baseUrl);
		
		
//		//perform first dummy test to get into mage report result page
		driver.findElement(By.xpath("/html/body/div[1]/main/form/input")).sendKeys(firstWebsite);
		driver.findElement(By.xpath("/html/body/div[1]/main/form/button")).click();
		
//		//assert results		
//		Boolean resultPage = driver.findElement(By.xpath("//*[@id='app']/main/div[2]/div/div[2]/section/div[2]/h4/span[1]/span[2]")).isDisplayed();		
//		Assert.assertTrue(resultPage);
		
		
		//get list into array
		try{			
			 FileReader fileReader = new FileReader(fileName);
			 BufferedReader bufferedReader = new BufferedReader(fileReader); 
			 
			 while((line = bufferedReader.readLine()) != null) {				 
				 websites.add(line);
			 }			 
			 
		}catch(FileNotFoundException err){			System.out.println("Unable to open file '" + fileName + "'");  
		} catch (IOException e) {			e.printStackTrace();
		}
		
		
		
		//perform test (loops through array)
		int i = 0;
		while (i < websites.size()) {
			
			//new test			
			driver.get(baseUrl + "scan/?s=" + websites.get(i)); 
			
			//for loggin purposes
			System.out.print(">>>\n>>>\n>>>*\t*\t*\t*\t" + websites.get(i) + "\t*\t*\t*\t*\nScanning ");
			
			//waits for the scan process
			try {
				while(driver.findElement(By.xpath("//*[@id='app']/main/div[2]/div/div[2]/section/div[2]/h4/span[1]/span[1]")).isDisplayed()){
					System.out.print(".");
					Thread.sleep(1000);
				}			                     
			} catch(Exception ex) {			    
			}
			
			//ensures that popup is closed if it appears
			try{
				driver.findElement(By.xpath("//*[@id='app']/main/div[3]/div/div/p/a")).click();			
			}catch(Exception e){ }


			//read results
			try{
				currResult = driver.findElement(By.xpath("//*[@id='app']/main/div[2]/div/div[2]/section/div[2]/h4")).getText();
				
				results.add(currResult);
				
				noOfResult = driver.findElements(By.xpath("//*[@id='app']/main/div[2]/div/section//article")).size();
				
				System.out.println("\n>>>" + websites.get(i) + "\t-\t" + currResult);
				
				for(int j = 1 ; j <= noOfResult ; j++){			
					currentPatch = driver.findElement(By.xpath("//*[@id='app']/main/div[2]/div/section//article["+j+"]/dl/dt")).getText();
					currentPatchStatus = driver.findElement(By.xpath("//*[@id='app']/main/div[2]/div/section//article["+j+"]/dl/dd")).getText();		
					
					//log only uninstalled patche
					if(!currentPatchStatus.toLowerCase().contains("installed") && 
							!currentPatchStatus.toLowerCase().contains("safe") && 
							!currentPatchStatus.toLowerCase().contains("protected") &&
							!currentPatchStatus.toLowerCase().contains("up to date") &&
							!currentPatchStatus.toLowerCase().contains("ok")){
						System.out.println(">>>" + currentPatch + "\t-\t" + currentPatchStatus);	
					}
				}
								
//				System.out.println(">>>\n>>>Result: " + websites.get(i) + "\t-\t" + currResult);							
								
			}catch(Exception e){ 
				
			}
			
			i++;
			driver.close();
			driver = new PhantomJSDriver();
			
		}
		
		
		
		//print results into text file
		try {
			PrintWriter writer = new PrintWriter("mageReport.txt","UTF-8");
			
			i = 0;
			while (i < results.size()) {			
			System.out.println(websites.get(i) +"\t-\t"+ results.get(i));			
			writer.println(websites.get(i) +"\t-\t"+ results.get(i));
			i++;
			}
			
		    writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		//write results to html file
		try {
			PrintWriter hwriter = new PrintWriter("mageReport.html","UTF-8");
			String textColor = "";
			String reportHeader = "<html>" + 
									"<head>" + 
										"<title>Mage Reports</title>" + 
										"<center><h1>Redbox Digital</h1>" + 
										"<h2>Mage Reports</h2></center>" + 
										"</head>" +
										"<body>" +
										"<table border='1'>" + 
										"<thead>" +
										"<tr>"+
										"<td>Projects Url</td>" +
										"<td>Status</td>" +
										"</tr>"+
										"</thead>";
			
			hwriter.println(reportHeader);
			
			
			
			i = 0;
			while (i < results.size()) {			
			
				if(results.get(i).toLowerCase().contains("low")){
					textColor = "orange";
				}else if(results.get(i).toLowerCase().contains("medium")){
					textColor = "maroon";
				}else if(results.get(i).toLowerCase().contains("high")){
					textColor = "red";
				}else{
					textColor = "green";
				}
				
				hwriter.println("<tr style=\"color:" + textColor + "\">");
				
				hwriter.println("<td>");
				hwriter.println("<a href=\"" + baseUrl + "scan/?s=" + websites.get(i) + "\">");
				hwriter.println(websites.get(i));
				hwriter.println("</a>");
				hwriter.println("</td>");
								
				hwriter.println("<td>");
				hwriter.println(results.get(i));
				hwriter.println("</td>");
				
				hwriter.println("</tr>");
				i++;
			}
			
			String reportFooter = "</table>" +
								"</body>" +
							"</html>";
			
			hwriter.println(reportFooter);
			
		    hwriter.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
