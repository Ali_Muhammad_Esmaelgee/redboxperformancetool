package checkRestrictedFolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class CheckRestrictedFolder {

	static WebDriver driver = null;
	static DesiredCapabilities cap;
	static File scrFile,fileUrls;
	static String fileName = "fileUrls.txt";
	
	
	public static void main(String[] args) {
		
		
		String url;
		
		cap = new DesiredCapabilities();
		cap.setJavascriptEnabled(true);
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "../phantomjs-2.1.1-windows/bin/phantomjs.exe");
		String[] PhantomArgs = new String[]{
			"--webdriver-loglevel=NONE"	
		};
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, PhantomArgs);
		
		
		int i=0;
		
		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader buffer = new BufferedReader(fileReader);
			
			//loop through websites
			while((url=buffer.readLine()) != null){
				
				System.out.println(">>>"+url);		
				checkFolder(url,i,"rss");				
				checkFolder(url,i,"downloader");
				System.out.println("\n");
				i++;
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static void checkFolder(String url, int i, String folder){
		
		if(driver != null && i%10 == 0 ){
			driver.quit();
			driver = null;
		}
		
		if(driver == null){
			driver = new PhantomJSDriver(cap);
		}
		
		
		try{
			 driver.get(url+"/"+folder);
			 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}catch(org.openqa.selenium.remote.UnreachableBrowserException e){
			System.out.println(">>>PhantomJs Crashed. Testing this Site again" + url);
			driver = null;
			checkFolder(url,i,folder);
		}

		
		//Assert that title contain 404
		try{
			Assert.assertTrue((driver.getTitle()).toLowerCase().contains("403"));
			System.out.println(">>>"+folder+"\t - \tProtected");
			
		}catch(Exception e){
			
			System.out.println(">>>"+folder+"\t - \tNOT PROTECTED (Screenshot: "+folder+"_"+i+".jpg)");
			
			//in case of error take picture
			try {
				scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(folder+"_"+i+".jpg"), true);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();				
			}
			
		}catch(java.lang.AssertionError e3){	
			
			System.out.println(">>>"+folder+"\t - \tNOT PROTECTED (Screenshot: "+folder+"_"+i+".jpg)");
			
			//in case of error take picture
			try {
				scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(folder+"_"+i+".jpg"), true);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				
			}					
		}
		
		
		
		
	}

}
